# Witify VueJS Starter Template
Witify VueJs Starter Template

##[Live Demo](https://vue-template.witify.io)

## SASS features
* Bootstrap
* Basic scss files
* Bourbon Sass Library
* Animations and transitions

## Multilingual
* Langage based routing with vue-router
* Translation with vue-i18n

## Webpack Build
* Page prerender for SEO with spa-prerender
* Images minification (PNG, JPG, SVG)
* JS minification
* SASS minification

## Vuex State
* Global attributes to facilitate animation design
* Height
* Width
* Scroll position
* Scroll direction

## Contact form
* Contact form using Mailchimp

## Prerender
To prerender specific pages, go to webpack.config.js and update the routes array.
A class is provided to generate all URLs in src/route/routeList.

## Todo in near future
* Validations on contact form with vue-validator 2.0
* Onion Layers Route Engine
