export default {

	/**
	 * Title of the website
	 * @type {String}
	 */
	title: 'Witify',

	/**
	 * Default Language
	 * @type {String}
	 */
	lang: 'en',

	/**
	 * Multi-language
	 * Toggles the language menu
	 * @type {Bool}
	 */
	multiLang: true,

	/**
	 * Emails for the Contact Form
	 * @type {Object}
	 */
	emails: {
		0: 'info@witify.io'
	},

	/**
	 * Phone Numbers to contact
	 * @type {Object}
	 */
	phones: {
		0: "(438) 403-9972"
	},

	/**
	 * Business Adress
	 * @type {Object}
	 */
	adress: {
		adress: "333 Sherbrooke E, 705",
		city: "Montréal (QC)",
		zipcode: "H2X 4E3",
		country: "Canada"
	}
}