/** NPM Packages */
import Vue from 'vue'
import Vuex from 'vuex'
import VueHead from 'vue-head'
import VueRouter from 'vue-router'
import VueI18n from 'vue-i18n'
import VueResource from 'vue-resource'

/** Vue Plugins */
Vue.use(VueHead)
Vue.use(VueRouter)
Vue.use(VueI18n)
Vue.use(VueResource)

/** Custom Plugins **/
import router from './route/router'
import store from './store'
import Config from './config'

/** Events */
import Scroll from './events/scroll'
import Ready from './events/ready'
import Resize from './events/resize'

/** Set Locales */
import locales from './lang/locales'
Object.keys(locales).forEach(function (lang) {
	Vue.locale(lang, locales[lang])
});

/** Global Components */
import VScroll from './components/partials/Scroll.vue'
Vue.component('v-scroll', VScroll)

/** SCSS import */
require('./assets/sass/main.scss')

/** Root App Component */
import App from './App.vue'

// Show all routes
// import routes from './route/list'

/** Main VueJs Instance */
new Vue({
	el: '#app',
	router,
	store,
	data() {
		return {
			config: Config
		}
	},
	beforeMount() {
		// Disable stylesheet is dev mode
		if((process.env.NODE_ENV || 'development') == 'development') {
			document.getElementById('css-bundle').href = "#";
		}
	},
	mounted() {
		// On first load
		Ready(this.$route);
	
		// On resize
		window.onresize = function() {
			Resize()
		}

		// On scroll
		document.onscroll = function() {
			Scroll()
		}

		// Show all routes
		//console.log(routes);
	},
	watch: {
		'$route': function (to, from) {
			// After page change
			Ready(to);
		}
	},
	render: h => h(App)
})