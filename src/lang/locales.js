export default {
	"fr": {
		"nav": {
			"home": "Accueil",
			"about": "À propos",
			"contact": "Nous joindre",
		},
		"home": {
			"title": "Vue Design",
			"subtitle": "Vue framework for designers",
		},
		"about": {
			"title": "À propos de Witify",
			"subtitle": "Nous sommes une équipe équilibrée, dynamique et audacieuse qui confectionne des produits digitaux d’une grande qualité pour le web.",
		},
		"contact": {
			"title": "Nous joindre",
			"form": {
				"name": "Nom",
				"email": "Courriel",
				"phone": "Téléphone",
				"message": "Message",
				"type": "Type de projet",
				"service": "Service",
				"send": "Envoyer",
			},
			"coordinates": "Coordonnées",
			"address": "Adresse",
			"email": "Courriel",
			"phone": "Téléphone",
			"success": "Votre message a été envoyé avec succès. Nous allons vous répondre aussi vite que possible.",
			"error": "Veuillez réessayer plus tard. Vous pouvez nous rejoindre via 'francois@witify.io'"
		},
		"footer": {
			"proudly": "Site développé par Witify"
		}
	},
	"en": {
		"nav": {
			"home": "Home",
			"about": "About",
			"contact": "Contact us",
		},
		"home": {
			"title": "Witify",
			"subtitle": "Vue framework for designers",
		},
		"about": {
			"title": "About Witify",
			"subtitle": "We are a balanced, dynamic and bold team that manufactures high-quality digital products for the web.",
		},
		"contact": {
			"title": "Contact Us",
			"form": {
				"name": "Name",
				"email": "Email",
				"phone": "Phone",
				"message": "Message",
				"type": "Project Type",
				"service": "Service",
				"send": "Send",
			},
			"coordinates": "Coordonninates",
			"address": "Adress",
			"email": "Email",
			"phone": "Phone",
			"success": "Your message has been sent successfully. We will respond as quickly as possible.",
			"error": "Please try again later. You may try to reach us via 'info@witify.io'"
		},
		"footer": {
			"proudly": "Website created by Witify"
		}
	},
}